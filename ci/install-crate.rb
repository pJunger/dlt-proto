#!/usr/bin/env ruby

require 'open3'

class Output
  attr_reader :status, :output
  def initialize(status, output)
    @status = status
    @output = output
  end

  def success?
    @status.success?
  end
end

def update_crate(name)
  Open3.popen2e('cargo', 'install-update', name) do |_,out,thread|
    puts "Updating #{name}"
    output = out.map {|line|
      puts line
      line
    }
    Output.new(thread.value, output)
  end
end

def update_crate2(name)
  output = `cargo install-update "#{name}" 2>&1`
  Output.new($?, output)
end

def install_crate_from_scratch(name)
  Open3.popen2e('cargo', 'install', name) do |_,out,thread|
    puts "Installing #{name}"
    output = out.map {|line|
      puts line
      out
    }
    Output.new(thread.value, output)
  end
end

def install_crate_from_scratch2(name)
  output =  `cargo install "#{name}" 2>&1`
  Output.new($?, output)
end

def install_crate(name)
  output = install_crate_from_scratch(name)

  return update_crate(name) unless output.success?
  output
end

def install_crates(names)
  output = install_crate('cargo-update')
  for name in names
    output = install_crate(name)
    return output unless output.success?
  end
  output
end

if __FILE__ == $0
  exit install_crates(ARGV).status.exitstatus
end