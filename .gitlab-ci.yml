default:
  image: rust:latest

# Setup a cache to cache job parts between jobs to ensure faster builds
cache:
  key: "$CI_JOB_NAME"
  untracked: true
  paths:
  - cargo/
  - target/
  - apt-cache/

.cargo_build_template: &cargo_build
  script:
    - rustc --version && cargo --version
    - cargo build

.cargo_test_template: &cargo_test
  script:
    - rustc --version && cargo --version
    - cargo test --verbose

# Set any required environment variables here
variables:
  APT_CACHE_DIR: apt-cache
  CARGO_HOME: $CI_PROJECT_DIR/cargo
  RUST_BACKTRACE: FULL

# Do any pre-flight requirements here, such as updating $PATH installing dependencies
before_script:
  - export PATH="$CI_PROJECT_DIR/cargo/bin:$PATH"

stages:
  - lint
  - build
  - test
  - artifacts
  - deploy

# Always want to run rustfmt and clippy against our tests, to ensure that
# we aren't using any anti-patterns or failing to follow our style guide
lint:rustfmt:
  stage: lint
  needs: []
  before_script: 
    - rustup component add rustfmt
  script: cargo fmt -- --check
  
lint:clippy:
  stage: lint
  needs: []
  before_script: 
    - rustup component add clippy
  script: cargo clippy -- -D warnings # Turn all warnings into errors

build:linux:stable:
  stage: build
  needs: []
  <<: *cargo_build
  
build:linux:stable:nostd:
  stage: build
  needs: []
  script:
    - rustc --version && cargo --version
    - cargo build --no-default-features
  
build:linux:nightly:
  stage: build
  needs: []
  image: rustlang/rust:nightly
  allow_failure: true
  <<: *cargo_build
  
build:web:stable:
  stage: build
  needs: []
  before_script:
    - export PATH="$CI_PROJECT_DIR/cargo/bin:$PATH"
    - rustup target add wasm32-unknown-unknown
  script: cargo build --target wasm32-unknown-unknown
  
build:redox:
  stage: build
  needs: []
  image: redoxos/redoxer
  script: redoxer build --verbose
  
# The following test: stages inherit from the test template above and
# configure the image used for the various Rust release trains
test:linux:stable:
  stage: test
  needs:
   - job: build:linux:stable
     artifacts: false
  <<: *cargo_test

test:linux:nightly:
  stage: test
  needs:
   - job: build:linux:nightly
     artifacts: false
  image: rustlang/rust:nightly
  cache:
    key: "$CI_JOB_NAME"
    untracked: true
    paths:
      - cargo/
      - apt-cache/
  allow_failure: true
  <<: *cargo_test

cov:linux:nightly:
  stage: artifacts
  needs:
   - job: test:linux:nightly
     artifacts: false
  image: rustlang/rust:nightly
  cache:
    key: "$CI_JOB_NAME"
    untracked: true
    paths:
      - cargo/
      - apt-cache/
  before_script:
    - export PATH="$CI_PROJECT_DIR/cargo/bin:$PATH"
    - mkdir -pv $APT_CACHE_DIR
    - rustup component add llvm-tools-preview
    - mkdir -pv $APT_CACHE_DIR
    - apt-get -qq update
    - apt-get -qq -o dir::cache::archives="$APT_CACHE_DIR" install -y
      ruby jq lld
    - ./ci/install-crate.rb rustfilt cargo-binutils
    - cargo cov --version
    - cargo cov -- --version
  script:
    - ls -la ci
    - ci/coverage.sh
  coverage: '/TOTAL\s+(?:\d+)\s+(?:\d+)\s+(?:[0-9.]+%)\s+(?:\d+)\s+(?:\d+)\s+(?:[0-9.]+%)\s+\d+\s+\d+\s+([0-9.]+)%$/'
  artifacts:
    paths:
      - target/cov
    when: on_success
    expire_in: 1 day

docs:
  stage: artifacts
  needs: []
  script: cargo doc
  artifacts:
    paths:
      - target/doc/dlt_proto
    when: on_success
    expire_in: 1 day

pages:
  stage: deploy
  only:
    - master
  needs:
    - job: cov:linux:nightly
      artifacts: true
    - job: docs
      artifacts: true
  image: alpine:latest
  cache:
    key: "$CI_JOB_NAME"
    untracked: true
    paths:
      - cargo/
      - apt-cache/
  before_script: 
    - mkdir -p public/
  script:
    - rm -rf public/cov public/doc
    - cp -r target/cov/ public/
    - cp -r target/doc/dlt_proto/ public/doc/
    - cp ci/index.html public/
  artifacts:
    paths:
      - public
    when: on_success
