# DLT proto

A simple to use parser for the AUTOSAR-DLT protocol.

Currently implements only parsing a DLT header.

## What is DLT

DLT is short for "Diagnostic Log and Trace" and is a protocol that is used mainly in automotive ECUs.
You can find more information about it at https://at.projects.genivi.org/wiki/display/PROJ/Diagnostic+Log+and+Trace
or https://www.autosar.org/.

## Example:

```rust
use dlt_proto;

let buffer = vec![1,2,3,4,5,6];

let (payload_buffer, header) = dlt_proto::DltHeader::parse(&buffer)?;
```