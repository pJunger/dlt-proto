use crate::types::{
    AppTraceMessageTypeInfo, CtrlMessageTypeInfo, LogMessageTypeInfo, NwTraceMessageTypeInfo,
};
use num_enum::TryFromPrimitiveError;

#[derive(Debug, derive_more::From, derive_more::Display)]
pub enum Error {
    #[display(fmt = "Invalid MTIN (Log): {_0}")]
    LogMessageTypeInfo(TryFromPrimitiveError<LogMessageTypeInfo>),
    #[display(fmt = "Invalid MTIN (App): {_0}")]
    AppTraceMessageTypeInfo(TryFromPrimitiveError<AppTraceMessageTypeInfo>),
    #[display(fmt = "Invalid MTIN (NW): {_0}")]
    NwTraceMessageTypeInfo(TryFromPrimitiveError<NwTraceMessageTypeInfo>),
    #[display(fmt = "Invalid MTIN (Ctrl): {_0}")]
    CtrlMessageTypeInfo(TryFromPrimitiveError<CtrlMessageTypeInfo>),
    #[display(fmt = "Invalid Byte: {_0}")]
    InvalidByte(u8),
}

#[cfg(feature = "std")]
impl ::std::error::Error for Error {
    fn description(&self) -> &str {
        ""
    }
}
