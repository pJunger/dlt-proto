//! A simple to use parser for the AUTOSAR-DLT protocol.
//!
//! # Example:
//!
//!```rust
//!use dlt_proto;
//!
//!let buffer = vec![41,81,234,191,218,33,38,49,18,216,94,64,60,223,222,31,223,44];
//!
//!let (buffer, header) = dlt_proto::DltHeader::parse(&buffer).unwrap();
//!```

#![deny(missing_docs)]
#![deny(rust_2018_compatibility)]
#![deny(rust_2018_idioms)]
#![deny(warnings)]
#![deny(clippy::all)]
#![deny(clippy::pedantic)]
#![allow(clippy::wildcard_imports)]
#![cfg_attr(not(feature = "std"), no_std)]

/// Contains all parsers for DLT
pub mod parser;
/// Contains all types defined in DLT
pub mod types;
/// Utility functionality for the crate
mod util;

#[doc(inline)]
pub use types::DltHeader;
