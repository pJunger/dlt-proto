//! Contains all types for DLT messages

use num_enum::{IntoPrimitive, TryFromPrimitive};
#[cfg(all(not(no_std), test))]
use proptest_derive::Arbitrary;

/// The complete DLT header
#[derive(Debug, Eq, PartialEq, Clone)]
#[cfg_attr(all(not(no_std), test), derive(Arbitrary))]
pub struct DltHeader {
    /// Mandatory DLT header
    pub standard_header: StandardHeader,
    /// Optional extended header
    pub extended_header: Option<ExtendedHeader>,
}

/// The mandatory header information
///
/// implements `PRS_Dlt_00458`
#[derive(Debug, Eq, PartialEq, Clone, Copy)]
#[cfg_attr(all(not(no_std), test), derive(Arbitrary))]
pub struct StandardHeader {
    /// Payload is encoded in big endian
    pub big_endian: bool,
    /// DLT message version
    #[cfg_attr(all(not(no_std), test), proptest(strategy = "0u8..8"))]
    pub version: u8,
    /// Message counter that starts at 0 and wraps around
    pub message_count: u8,
    /// Complete message length (including header)
    pub length: u16,
    /// The ecu from which the message originates
    pub ecu_id: Option<u32>,
    /// An optional session id
    pub session_id: Option<u32>,
    /// The timestamp starting from 0 at service start
    pub timestamp: Option<u32>,
}

/// The header type info (HTYP)
///
/// implements `PRS_Dlt_00094`
#[derive(Debug, Eq, PartialEq, Clone, Copy)]
#[cfg_attr(all(not(no_std), test), derive(Arbitrary))]
#[allow(clippy::struct_excessive_bools)]
pub(crate) struct HeaderType {
    /// True if an extended header is sent
    ///
    /// implements `PRS_Dlt_00602`, `PRS_Dlt_00603`
    pub use_extended_header: bool,
    /// True if the payload is in big endian format
    ///
    /// implements `PRS_Dlt_00604`, `PRS_Dlt_00605`
    pub most_significant_byte_first: bool,
    /// True if an ECU ID is sent
    ///
    /// implements `PRS_Dlt_00606`, `PRS_Dlt_00607`
    pub with_ecu_id: bool,
    /// True if a session ID is sent
    ///
    /// implements `PRS_Dlt_00608`, `PRS_Dlt_00609`
    pub with_session_id: bool,
    /// True if a timestamp is sent
    ///
    /// implements `PRS_Dlt_00610`, `PRS_Dlt_00611`
    pub with_timestamp: bool,
    /// Used DLT version
    ///
    /// implements `PRS_Dlt_00612`
    #[cfg_attr(all(not(no_std), test), proptest(strategy = "0u8..8"))]
    pub version: u8,
}

/// The optional extended header information
/// Needed for verbose messages
///
/// implements `PRS_Dlt_00617`
#[derive(Debug, Eq, PartialEq, Clone, Copy)]
#[cfg_attr(all(not(no_std), test), derive(Arbitrary))]
pub struct ExtendedHeader {
    /// True if the message is in 'verbose' format, false if 'non-verbose'
    pub verbose: bool,
    /// The type (and severity) of message
    pub message_type: MessageType,
    /// The number of arguments contained in the message
    pub num_arguments: u8,
    /// The application from which the message originates
    pub application_id: u32,
    /// The log context from which the message originates
    pub context_id: u32,
}

/// The DLT message type (MSTP)
///
/// implements `PRS_Dlt_00120`
#[derive(Debug, Eq, PartialEq, Clone, Copy)]
#[cfg_attr(all(not(no_std), test), derive(Arbitrary))]
pub enum MessageType {
    /// Log Message with severity
    ///
    /// encoded as 0x0
    Log(LogMessageTypeInfo),
    /// Application tracing with trace type
    ///
    /// encoded as 0x1
    AppTrace(AppTraceMessageTypeInfo),
    /// Network Trace with bus type
    ///
    /// encoded as 0x2
    NetworkTrace(NwTraceMessageTypeInfo),
    /// Control Message (request or response)
    ///
    /// encoded as 0x3
    Control(CtrlMessageTypeInfo),
}

/// The Dlt message type info types (MTIN)
///
/// implements `PRS_Dlt_00619`
#[derive(Debug, Eq, PartialEq, Clone, Copy, TryFromPrimitive, IntoPrimitive)]
#[cfg_attr(all(not(no_std), test), derive(Arbitrary))]
#[repr(u8)]
pub enum LogMessageTypeInfo {
    /// Fatal system error
    Fatal = 0x1,
    /// Application error
    Error = 0x2,
    /// Correct behavior cannot be ensured
    Warn = 0x3,
    /// Information messages
    Info = 0x4,
    /// Debug messages
    Debug = 0x5,
    /// Verbose Debug messages
    Verbose = 0x6,
}

/// The Dlt message type info types (MTIN)
///
/// implements `PRS_Dlt_00620`
#[derive(Debug, Eq, PartialEq, Clone, Copy, TryFromPrimitive, IntoPrimitive)]
#[cfg_attr(all(not(no_std), test), derive(Arbitrary))]
#[repr(u8)]
pub enum AppTraceMessageTypeInfo {
    /// Value of a variable
    Variable = 0x1,
    /// Call of a function
    FunctionIn = 0x2,
    /// Return of a function
    FunctionOut = 0x3,
    /// State of a state machine
    State = 0x4,
    /// Virtual functional bus (RTE) events
    Vfb = 0x5,
}

/// The Dlt message type info types (MTIN)
///
/// implements `PRS_Dlt_00621`
#[derive(Debug, Eq, PartialEq, Clone, Copy, TryFromPrimitive, IntoPrimitive)]
#[cfg_attr(all(not(no_std), test), derive(Arbitrary))]
#[repr(u8)]
pub enum NwTraceMessageTypeInfo {
    /// Inter process communication
    Ipc = 0x1,
    /// CAN bus
    Can = 0x2,
    /// FlexRay bus
    FlexRay = 0x3,
    /// MOST bus
    Most = 0x4,
    /// Ethernet
    Ethernet = 0x5,
    /// SOME/IP
    SomeIp = 0x6,
}

/// The Dlt message type info types (MTIN)
///
/// implements `PRS_Dlt_00622`
#[derive(Debug, Eq, PartialEq, Clone, Copy, TryFromPrimitive, IntoPrimitive)]
#[cfg_attr(all(not(no_std), test), derive(Arbitrary))]
#[repr(u8)]
pub enum CtrlMessageTypeInfo {
    /// A control message request from a tester / viewer
    Request = 0x1,
    /// A control message response from the logger
    Response = 0x2,
}
