use core::convert::TryFrom;

use bit_field::BitField;
use nom::{
    combinator::{cond, map, map_res},
    number::streaming::{be_u16, be_u32, be_u8},
    sequence::*,
    IResult,
};

use crate::types::*;
use crate::util::error::Error;

/// Buffer type for parsing
type Span<'a> = &'a [u8];

impl DltHeader {
    /// Parses the complete DLT header
    ///
    /// # Errors
    ///
    /// Will return an error if the standard or extended headers cannot be parsed
    pub fn parse(input: Span<'_>) -> IResult<Span<'_>, Self> {
        let (input, htyp) = HeaderType::parse(input)?;
        let is_extended = htyp.use_extended_header;
        let (input, standard_header) = StandardHeader::parse(htyp, input)?;
        let (input, extended_header) = cond(is_extended, ExtendedHeader::parse)(input)?;
        Ok((
            input,
            Self {
                standard_header,
                extended_header,
            },
        ))
    }
}

impl StandardHeader {
    /// Parses the complete standard header
    fn parse(htyp: HeaderType, input: Span<'_>) -> IResult<Span<'_>, Self> {
        map(
            tuple((
                be_u8,                              // message count
                be_u16,                             // message length
                cond(htyp.with_ecu_id, be_u32),     // ecu id
                cond(htyp.with_session_id, be_u32), // session id
                cond(htyp.with_timestamp, be_u32),  // time stamp
            )),
            Self::from_header_type(htyp),
        )(input)
    }

    fn from_header_type(
        htyp: HeaderType,
    ) -> impl Fn((u8, u16, Option<u32>, Option<u32>, Option<u32>)) -> Self {
        move |(message_count, length, ecu_id, session_id, timestamp)| Self {
            big_endian: htyp.most_significant_byte_first,
            version: htyp.version,
            message_count,
            length,
            ecu_id,
            session_id,
            timestamp,
        }
    }
}

impl HeaderType {
    /// Parses the header type
    fn parse(input: Span<'_>) -> IResult<Span<'_>, Self> {
        let byte_parser = |byte: u8| Self {
            use_extended_header: byte.get_bit(0),
            most_significant_byte_first: byte.get_bit(1),
            with_ecu_id: byte.get_bit(2),
            with_session_id: byte.get_bit(3),
            with_timestamp: byte.get_bit(4),
            version: byte.get_bits(5..8),
        };
        map(be_u8, byte_parser)(input)
    }
}

impl ExtendedHeader {
    /// Parses the complete extended header
    fn parse(input: Span<'_>) -> IResult<Span<'_>, Self> {
        let (
            input,
            MessageInfo {
                verbose,
                message_type,
            },
        ) = MessageInfo::parse(input)?;

        let (input, num_arguments) = be_u8(input)?;
        let (input, application_id) = be_u32(input)?;
        let (input, context_id) = be_u32(input)?;

        Ok((
            input,
            Self {
                verbose,
                message_type,
                num_arguments,
                application_id,
                context_id,
            },
        ))
    }
}

/// The extended header message info (MSIN)
struct MessageInfo {
    verbose: bool,             // VERB
    message_type: MessageType, // MSTP + MTIN
}

impl MessageInfo {
    fn parse(input: Span<'_>) -> IResult<Span<'_>, Self> {
        let byte_parser = |byte: u8| {
            let mstp = byte.get_bits(1..4);
            let mtin = byte.get_bits(4..8);
            let verbose = byte.get_bit(0);
            MessageType::from_type_info(mstp, mtin).map(|message_type| Self {
                verbose,
                message_type,
            })
        };
        map_res(be_u8, byte_parser)(input)
    }
}

impl MessageType {
    fn from_type_info(mstp: u8, mtin: u8) -> Result<Self, Error> {
        match mstp {
            0x0 => Ok(Self::Log(LogMessageTypeInfo::try_from(mtin)?)),
            0x1 => Ok(Self::AppTrace(AppTraceMessageTypeInfo::try_from(mtin)?)),
            0x2 => Ok(Self::NetworkTrace(NwTraceMessageTypeInfo::try_from(mtin)?)),
            0x3 => Ok(Self::Control(CtrlMessageTypeInfo::try_from(mtin)?)),
            invalid => Err(Error::InvalidByte(invalid)),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use bit_field::BitField;
    use byteorder::{BigEndian, WriteBytesExt};
    use proptest::prelude::*;

    proptest! {
        # [test]
        fn header_type_can_be_parsed(input_htyp in any::<HeaderType>()) {
            let mut htyp = 0_u8;
            htyp.set_bit(0, input_htyp.use_extended_header)
                          .set_bit(1, input_htyp.most_significant_byte_first)
                          .set_bit(2, input_htyp.with_ecu_id)
                          .set_bit(3, input_htyp.with_session_id)
                          .set_bit(4, input_htyp.with_timestamp)
                          .set_bits(5..=7, input_htyp.version);
            println!("Expected: {input_htyp:?}");

            let buffer: Vec<u8> = vec![htyp];

            let (_, parsed_htyp) = HeaderType::parse(&buffer)?;
            println!("Actual: {parsed_htyp:?}");

            assert_eq!(parsed_htyp, input_htyp);
        }

        # [test]
        fn dlt_headers_can_be_parsed(std in any::<StandardHeader>(),
                                     ext in any::<Option<ExtendedHeader>>()) {
            let mut htyp = 0_u8;
            htyp.set_bit(0, ext.is_some())
                .set_bit(1, std.big_endian)
                .set_bit(2, std.ecu_id.is_some())
                .set_bit(3, std.session_id.is_some())
                .set_bit(4, std.timestamp.is_some())
                .set_bits(5..=7, std.version);

            let mut buffer: Vec<u8> = vec![htyp, std.message_count];

            buffer.write_u16::<BigEndian>(std.length).expect("Writing length failed");

            if let Some(ecu_id) = std.ecu_id {
                buffer.write_u32::<BigEndian>(ecu_id).expect("Writing ECU ID failed");
            }

            if let Some(session_id) = std.session_id {
                buffer.write_u32::<BigEndian>(session_id).expect("Writing session ID failed");
            }

            if let Some(timestamp) = std.timestamp {
                buffer.write_u32::<BigEndian>(timestamp).expect("Writing time stamp failed");
            }

            if let Some(ext) = ext {
                let (mstp, mtin): (u8, u8) = match ext.message_type {
                    MessageType::Log(level) => (0x0, level.into()),
                    MessageType::AppTrace(kind) => (0x1, kind.into()),
                    MessageType::NetworkTrace(source) => (0x2, source.into()),
                    MessageType::Control(msg) => (0x3, msg.into()),
                };
                let mut msin = 0_u8;
                msin.set_bit(0, ext.verbose)
                    .set_bits(1..=3, mstp)
                    .set_bits(4..=7, mtin);
                buffer.write_u8(msin).expect("Writing msin failed");
                buffer.write_u8(ext.num_arguments).expect("Writing num_arguments failed");
                buffer.write_u32::<BigEndian>(ext.application_id).expect("Writing app id failed");
                buffer.write_u32::<BigEndian>(ext.context_id).expect("Writing ctx id failed");
            }

            let (_, header) = DltHeader::parse(&buffer)?;

            assert_eq!(header.standard_header, std);
            assert_eq!(header.extended_header, ext);
        }
    }
}
